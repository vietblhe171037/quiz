﻿create database Quiz_Practice
GO
use Quiz_Practice
GO

CREATE TABLE [user] (
	id integer identity(1,1) NOT NULL,
	account varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	role_id integer NOT NULL,
	[status] bit NOT NULL,
  CONSTRAINT [PK_USER] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [role] (
	id integer identity(1,1) NOT NULL,
	name varchar(10) NOT NULL,
  CONSTRAINT [PK_ROLE] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [dimension_type] (
	id integer identity(1,1) NOT NULL,
	name varchar(20) NOT NULL,
  CONSTRAINT [PK_DIMENSION_TYPE] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [subject] (
	id integer identity(1,1) NOT NULL,
	illustration varchar(1000),
	name varchar(20) NOT NULL,
	category_id integer NOT NULL,
	status bit NOT NULL,
	description text NOT NULL,
	author_id integer NOT NULL,
	modified date,
	featured bit NOT NULL,
  CONSTRAINT [PK_SUBJECT] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [dimension] (
	id integer identity(1,1) NOT NULL,
	name varchar(20) NOT NULL,
	[type_id] integer NOT NULL,
	description text NOT NULL,
  CONSTRAINT [PK_DIMENSION] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)
)

GO
CREATE TABLE [subject_dimension] (
	subject_id integer NOT NULL,
	dimension_id integer NOT NULL,
  CONSTRAINT [PK_SUBJECT_DIMENSION] PRIMARY KEY CLUSTERED
  (
  [subject_id] ASC, dimension_id
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [subject_category] (
	id integer identity(1,1) NOT NULL,
	name varchar(20) NOT NULL,
  CONSTRAINT [PK_SUBJECT_CATEGORY] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [price_package] (
	id integer identity(1,1) NOT NULL,
	[name] varchar(255) NOT NULL,
	[description] text,
	duration integer NOT NULL,
	price money NOT NULL,
	sale decimal NOT NULL,
	[status] bit NOT NULL,
  CONSTRAINT [PK_PRICE_PACKAGE] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)

CREATE TABLE [subject_price_package] (
	subject_id integer NOT NULL,
	price_package_id integer NOT NULL,
	status bit NOT NULL,
  CONSTRAINT [PK_SUBJECT_PRICE_PACKAGE] PRIMARY KEY CLUSTERED
  (
  [subject_id] ASC, price_package_id
  ) WITH (IGNORE_DUP_KEY = OFF)
)

GO
CREATE TABLE [registration] (
	id integer identity(1,1) NOT NULL,
	subject_id integer NOT NULL,
	price_package_id integer NOT NULL,
	[user_id] integer NOT NULL,
	created datetime NOT NULL,
	category_id integer NOT NULL,
	subject_name varchar(20) NOT NULL,
	[status] bit NOT NULL,
  CONSTRAINT [PK_REGISTRATION] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [exam] (
	id integer identity(1,1) NOT NULL,
	name varchar(255) NOT NULL,
	subject_id integer NOT NULL,
	[level] varchar NOT NULL,
	duration time NOT NULL,
	pass_rate decimal NOT NULL,
	number_of_question integer NOT NULL,
	description text,
	created datetime NOT NULL,
	mode bit NOT NULL,
  CONSTRAINT [PK_EXAM] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [exam_user] (
	user_id integer NOT NULL,
	exam_id integer NOT NULL,
  CONSTRAINT [PK_EXAM_USER] PRIMARY KEY CLUSTERED
  (
  [user_id] , [exam_id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF), 
)

GO
CREATE TABLE [lesson] (
	id integer identity(1,1) NOT NULL,
	subject_id integer NOT NULL,
	topic_id integer NOT NULL,
	name varchar(20) NOT NULL,
	[type_id] integer NOT NULL,
	[order] integer NOT NULL,
	video_link nvarchar(255) NOT NULL,
	html_content text,
	[status] bit, 
	exam_id integer,
  CONSTRAINT [PK_LESSON] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [lesson_topic] (
	id integer identity(1,1) NOT NULL,
	name varchar(20) NOT NULL,
  CONSTRAINT [PK_LESSON_TOPIC] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [lesson_type] (
	id integer identity(1,1) NOT NULL,
	name varchar(255) NOT NULL,
  CONSTRAINT [PK_LESSON_TYPE] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [question] (
	id integer identity(1,1) NOT NULL,
	subject_id integer NOT NULL,
	lesson_id integer NOT NULL,
	content text NOT NULL,
	option_a varchar(255) NOT NULL,
	option_b varchar(255) NOT NULL,
	option_c varchar(255) NOT NULL,
	option_d varchar(255) NOT NULL,
	answer varchar(255) NOT NULL,
	answer_explaination text,
	level varchar(255) NOT NULL,
	status bit NOT NULL,
	created datetime NOT NULL,
	modified datetime,
  CONSTRAINT [PK_QUESTION] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)
)

GO
CREATE TABLE [question_dimension] (
	question_id integer NOT NULL,
	dimension_id integer NOT NULL,
  CONSTRAINT [PK_QUESTION_DIMENSION] PRIMARY KEY CLUSTERED
  (
  [question_id] ASC, [dimension_id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [question_exam] (
	exam_id integer NOT NULL,
	question_id integer NOT NULL,
	question_order integer NOT NULL,
	marks_allocated decimal NOT NULL,
  CONSTRAINT [PK_QUESTION_EXAM] PRIMARY KEY CLUSTERED
  (
  [exam_id] ASC, [question_id]
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [user_profile] (
	user_id integer NOT NULL,
	avatar varchar(255),
	full_name nvarchar(255) NOT NULL,
	gender bit NOT NULL,
	dob date NOT NULL,
	phone_number varchar(20) NOT NULL,
	created datetime NOT NULL,
	modified datetime,
  CONSTRAINT [PK_USER_PROFILE] PRIMARY KEY CLUSTERED
  (
  [user_id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [blog_category] (
	id integer identity(1,1) NOT NULL,
	name varchar(20) NOT NULL,
  CONSTRAINT [PK_BlOG_CATEGORY] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)

GO
CREATE TABLE [blog] (
	id integer identity(1,1) NOT NULL,
	thumbnail varchar(255) NOT NULL,
	author_id integer NOT NULL,
	title varchar(255) NOT NULL,
	category_id integer NOT NULL,
	flag varchar(255),
	[status] bit NOT NULL,
	brief_info text NOT NULL,
	[view] bigint NOT NULL,
	content text NOT NULL,
	created datetime NOT NULL,
	modified datetime,
  CONSTRAINT [PK_BLOG] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [slider] (
	id integer identity(1,1) NOT NULL,
	title varchar(255) NOT NULL,
	[image] varchar(255) NOT NULL,
	backlink varchar(255) NOT NULL,
	status bit NOT NULL,
	note text NOT NULL,
	created datetime NOT NULL,
	modified datetime,
  CONSTRAINT [PK_SLIDER] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF),
)

GO

CREATE TABLE [attempt] (
	attempt_id integer NOT NULL,
	exam_id integer NOT NULL,
	question_id integer NOT NULL,
	user_id integer NOT NULL,
	marked bit,
	user_answer nvarchar(255),
	score decimal,
	created datetime NOT NULL,
  CONSTRAINT [PK_ATTEMPT] PRIMARY KEY CLUSTERED
  (
  attempt_id ASC, [exam_id], user_id, question_id
  ) WITH (IGNORE_DUP_KEY = OFF),
)

GO
CREATE TABLE [authorization] (
	id integer identity(1,1) NOT NULL,
	role_id integer NOT NULL,
	[url] text NOT NULL,
  CONSTRAINT [PK_AUTHORIZATION] PRIMARY KEY CLUSTERED
  (
	[id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF),
)

GO

ALTER TABLE [authorization]
ADD CONSTRAINT authorization_fk0 FOREIGN KEY (role_id)
REFERENCES [role] (id);	
GO

ALTER TABLE [question_dimension]
ADD CONSTRAINT question_dimension_fk0 FOREIGN KEY (question_id)
REFERENCES question (id);

GO

ALTER TABLE [question_dimension]
ADD CONSTRAINT question_dimension_fk1 FOREIGN KEY (dimension_id)
REFERENCES dimension (id);

GO

ALTER TABLE [subject_price_package]
ADD CONSTRAINT subject_price_package_fk0 FOREIGN KEY (subject_id)
REFERENCES subject (id);

GO

ALTER TABLE [subject_price_package]
ADD CONSTRAINT subject_price_package_fk1 FOREIGN KEY (price_package_id)
REFERENCES price_package (id);

GO

ALTER TABLE [attempt]
ADD CONSTRAINT attempt_fk0 FOREIGN KEY (exam_id)
REFERENCES exam (id);

GO

ALTER TABLE [attempt]
ADD CONSTRAINT attempt_fk1 FOREIGN KEY (question_id)
REFERENCES question (id);

GO

ALTER TABLE [attempt]
ADD CONSTRAINT attempt_fk2 FOREIGN KEY (user_id)
REFERENCES [user] (id);

GO

ALTER TABLE question_exam
ADD CONSTRAINT question_exam_fk1 FOREIGN KEY (question_id)
REFERENCES question (id);

GO

ALTER TABLE registration
ADD CONSTRAINT registration_fk2 FOREIGN KEY ([user_id])
REFERENCES [user] (id);

GO

ALTER TABLE registration
ADD CONSTRAINT registration_fk3 FOREIGN KEY ([category_id])
REFERENCES [subject_category] (id);

GO

ALTER TABLE exam_user
ADD CONSTRAINT exam_user_fk1 FOREIGN KEY ([user_id])
REFERENCES [user] (id);

GO

ALTER TABLE [subject_dimension]
ADD CONSTRAINT subject_dimension_fk1 FOREIGN KEY ([dimension_id])
REFERENCES [dimension] (id);

GO
ALTER TABLE [question]
ADD CONSTRAINT question_fk1 FOREIGN KEY ([lesson_id])
REFERENCES [lesson] (id);

GO
ALTER TABLE [user] WITH CHECK ADD CONSTRAINT [user_fk0] FOREIGN KEY ([role_id]) REFERENCES [role]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [user] CHECK CONSTRAINT [user_fk0]
GO
ALTER TABLE [user_profile] WITH CHECK ADD CONSTRAINT [user_profile_fk0] FOREIGN KEY ([user_id]) REFERENCES [user]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [user_profile] CHECK CONSTRAINT [user_profile_fk0]
GO


ALTER TABLE [subject] WITH CHECK ADD CONSTRAINT [subject_fk0] FOREIGN KEY ([category_id]) REFERENCES [subject_category]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [subject] CHECK CONSTRAINT [subject_fk0]
GO

ALTER TABLE [subject] WITH CHECK ADD CONSTRAINT [subject_fk1] FOREIGN KEY ([author_id]) REFERENCES [user]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [subject] CHECK CONSTRAINT [subject_fk1]
GO

ALTER TABLE [dimension] WITH CHECK ADD CONSTRAINT [dimension_fk0] FOREIGN KEY ([type_id]) REFERENCES [dimension_type]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dimension] CHECK CONSTRAINT [dimension_fk0]
GO

ALTER TABLE [subject_dimension] WITH CHECK ADD CONSTRAINT [subject_dimension_fk0] FOREIGN KEY ([subject_id]) REFERENCES [subject]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [subject_dimension] CHECK CONSTRAINT [subject_dimension_fk0]
GO

ALTER TABLE [registration] WITH CHECK ADD CONSTRAINT [registration_fk0] FOREIGN KEY ([subject_id]) REFERENCES [subject]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [registration] CHECK CONSTRAINT [registration_fk0]
GO
ALTER TABLE [registration] WITH CHECK ADD CONSTRAINT [registration_fk1] FOREIGN KEY ([price_package_id]) REFERENCES [price_package]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [registration] CHECK CONSTRAINT [registration_fk1]
GO

ALTER TABLE [exam] WITH CHECK ADD CONSTRAINT [exam_fk0] FOREIGN KEY ([subject_id]) REFERENCES [subject]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [exam] CHECK CONSTRAINT [exam_fk0]
GO

ALTER TABLE [exam_user] WITH CHECK ADD CONSTRAINT [exam_user_fk0] FOREIGN KEY ([exam_id]) REFERENCES [exam]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [exam_user] CHECK CONSTRAINT [exam_user_fk0]
GO

ALTER TABLE [lesson] WITH CHECK ADD CONSTRAINT [lesson_fk0] FOREIGN KEY ([subject_id]) REFERENCES [subject]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [lesson] CHECK CONSTRAINT [lesson_fk0]
GO

ALTER TABLE [lesson] WITH CHECK ADD CONSTRAINT [lesson_fk1] FOREIGN KEY ([type_id]) REFERENCES [lesson_type]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [lesson] CHECK CONSTRAINT [lesson_fk1]
GO

ALTER TABLE [lesson] WITH CHECK ADD CONSTRAINT [lesson_fk2] FOREIGN KEY ([topic_id]) REFERENCES [lesson_topic]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [lesson] CHECK CONSTRAINT [lesson_fk2]
GO

ALTER TABLE [question] WITH CHECK ADD CONSTRAINT [question_fk0] FOREIGN KEY ([subject_id]) REFERENCES [subject]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [question] CHECK CONSTRAINT [question_fk0]
GO

ALTER TABLE [blog] WITH CHECK ADD CONSTRAINT [blog_fk0] FOREIGN KEY ([author_id]) REFERENCES [user]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [blog] CHECK CONSTRAINT [blog_fk0]
GO

ALTER TABLE [blog] WITH CHECK ADD CONSTRAINT [blog_fk1] FOREIGN KEY ([category_id]) REFERENCES [blog_category]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [blog] CHECK CONSTRAINT [blog_fk1]
GO

ALTER TABLE [question_exam] WITH CHECK ADD CONSTRAINT [question_exam_fk0] FOREIGN KEY ([exam_id]) REFERENCES [exam]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [question_exam] CHECK CONSTRAINT [question_exam_fk0]
GO



INSERT INTO role(name)
VALUES ('Customer'),
	   ('Marketing'),
       ('Sale'),
       ('Expert'),
       ('Admin'),
	   ('Guest');
GO

INSERT INTO [user](account, password, role_id, status)
VALUES ('dungnpnhe171417@fpt.edu.vn', 123, 1, 1),
	   ('npndung@gmail.com', 123, 1, 1),
       ('dungnpn@gmail.com', 123, 1, 1),
       ('vuhieu2003hp@gmail.com', 123, 2, 1),
       ('hieuvn@gmail.com', 123, 2, 1),
	   ('vnhieu@gmail.com', 123, 2, 1),
	   ('hoangphucoh@gmail.com', 123, 3, 1),
	   ('phuclth@gmail.com', 123, 3, 1),
	   ('lthphuc@gmail.com', 123, 3, 1),
	   ('nguyenminhdai203@gmail.com', 123, 4, 1),
	   ('dainm@gmail.com', 123, 4, 1),
	   ('nmdai@gmail.com', 123, 4, 1),
	   ('builanviet@gmail.com', 123, 5, 1),
	   ('vietbl@gmail.com', 123, 5, 1),
	   ('blviet@gmail.com', 123, 5, 1);

GO

INSERT INTO [user_profile]([user_id], full_name, gender, dob, phone_number, created)
VALUES (1, N'Nguyễn Phạm Nam Dũng', 1, '2003-10-28', '0375470304' , GETDATE()),
	   (2, 'Nam Dung', 1, '1999-09-22', '0999999999' , GETDATE()),
	   (3, 'Nguyen Pham Nam Dung', 1, '2001-09-08', '0111111111' , GETDATE()),
	   (4, N'Vũ Ngọc Hiếu', 1, '2004-01-01', '0111111112' , GETDATE()),
       (5, 'Ngoc Hieu', 0, '2003-08-11', '06473835648' , GETDATE()),
	   (6, 'Vu Ngoc Hieu', 1, '2003-10-28', '0375470304' , GETDATE()),
	   (7, N'Liểu Triệu Hoàng Phúc', 1, '1999-09-22', '0999999999' , GETDATE()),
	   (8, 'Hoang Phuc', 1, '2001-09-08', '0111111111' , GETDATE()),
	   (9, 'Lieu Trieu Hoang Phuc', 1, '2004-01-01', '0111111112' , GETDATE()),
       (10, N'Nguyễn Minh Đại', 0, '2003-08-11', '06473835648' , GETDATE()),
	   (11, 'Minh Dai', 1, '2003-10-28', '0375470304' , GETDATE()),
	   (12, 'Nguyen Minh Dai', 1, '1999-09-22', '0999999999' , GETDATE()),
	   (13, N'Bùi Lân Việt', 1, '2001-09-08', '0111111111' , GETDATE()),
	   (14, 'Lan Viet', 1, '2004-01-01', '0111111112' , GETDATE()),
       (15, 'Bui Lan Viet', 0, '2003-08-11', '06473835648' , GETDATE());

GO

Insert INTO [slider] (title, [image], backlink, status, created, note) 
VALUES ('Youtube KTeam', '1687329303252.png', 'https://www.youtube.com/@KTeam', 1, GETDATE(), 'this is note for slider'),
	   ('Chegg', '07buAANY8wyfRXvDdbCtlw7-1.fit_scale.size_760x427.v1617389466.png', 'https://www.chegg.com/', 1, GETDATE(), 'this is note for slider'),
	   ('Coursera', '1687319042906.jpg', 'https://www.coursera.org/', 1, GETDATE(), 'this is note for slider'),
	   ('EdX', 'edx.jpg', 'https://www.edx.org/', 1, GETDATE(), 'this is note for slider'),
	   ('FAP', 'fap.jpg', 'https://fap.fpt.edu.vn/Default.aspx', 1, GETDATE(), 'this is note for slider'); 

GO
INSERT INTO [blog_category] (name) 
VALUES ('Science'),
	   ('Technology'),
	   ('Engineering'),
	   ('Mathematics'),
	   ('English');

GO

INSERT INTO [blog] ([view], thumbnail, author_id, title, category_id, flag, [status], content, created, brief_info, modified)
VALUES (10, '1687167470609.jpg', 4, 'Exploring the Enigmatic World of Quantum Computing', 1, 1, 1, 'Have you ever wondered what lies beyond the realm of classical computing? Brace yourself for a mind-bending journey into the enigmatic world of quantum computing. With their mind-boggling properties and immense potential, quantum computers are poised to revolutionize the way we solve complex problems.

Quantum computing harnesses the fundamental principles of quantum mechanics to perform computations. Unlike classical bits that can represent only a 0 or a 1, quantum bits, or qubits, can exist in a superposition of states, enabling parallel processing and exponentially increasing computational power.

The applications of quantum computing are far-reaching. From cryptography and drug discovery to optimization problems and artificial intelligence, this technology holds the key to solving problems that are currently beyond the capabilities of classical computers.

However, harnessing the power of quantum computing is no easy feat. Qubits are highly delicate and susceptible to interference, making error correction a monumental challenge. Scientists and researchers worldwide are tirelessly working to overcome these hurdles and bring quantum computing to its full potential.

As we venture into this uncharted territory, its important to keep in mind that quantum computing is still in its infancy. Many breakthroughs and advancements are yet to come, and the true extent of its impact remains to be seen. Nonetheless, the future looks promising, and we eagerly await the day when quantum computers become a ubiquitous tool in various fields.

So fasten your seatbelts and prepare to be awestruck as we delve deeper into the awe-inspiring world of quantum computing. The possibilities are boundless, and the journey is sure to be nothing short of extraordinary.', GETDATE(), 'Quantum computing is a revolutionary field with exponential computational power, utilizing quantum mechanics. It holds promise for solving complex problems, but challenges remain. Exciting breakthroughs are expected in the future.', GETDATE()),
	   (20, '1687168213752.jpg', 5, 'The Rise of Augmented Reality: Transforming the Way We Experience Technology', 2, 1, 1, 'In recent years, a groundbreaking technology has taken center stage and captured the imagination of both tech enthusiasts and everyday users alike. Augmented Reality (AR) has emerged as a game-changer, blurring the line between the real and digital worlds and revolutionizing the way we experience technology.

Augmented Reality seamlessly integrates virtual elements into our physical surroundings, enhancing our perception and interaction with the environment. From smartphone apps and smart glasses to industrial applications and entertainment experiences, AR has found its way into various domains, captivating users with its immersive and interactive capabilities.

One of the most well-known examples of AR is the popular mobile game Pokémon Go, which brought the concept to the mainstream. Players could see virtual creatures overlaid onto their real-world surroundings, fostering a new level of engagement and exploration.

Beyond gaming, AR has found practical applications across industries. From providing real-time information overlays for field technicians to enabling virtual try-on experiences for retail customers, this technology is reshaping the way businesses operate and deliver value to their customers.

As AR continues to evolve, the possibilities are expanding exponentially. From educational simulations and architectural visualization to medical training and remote collaboration, AR is poised to transform numerous aspects of our lives.

However, challenges persist, such as improving hardware capabilities, refining tracking and mapping technologies, and ensuring user privacy and security. Nonetheless, the trajectory of ARs development indicates a future where the line between the real and virtual worlds will become increasingly blurred.

So fasten your seatbelts and prepare for a world where imagination knows no bounds. Augmented Reality is poised to redefine how we perceive and interact with technology, opening up a realm of endless possibilities.', GETDATE(), 'Augmented Reality (AR) seamlessly integrates virtual elements into the real world, transforming our technology experiences. It has applications in gaming, retail, industry, education, and healthcare. Challenges exist, but ARs development is expanding rapidly, offering limitless possibilities in how we perceive and interact with technology.', GETDATE()),
	   (30, 'dubai.jpg', 6, 'The Engineering Marvels Shaping Our Future: A Glimpse into Innovative Technologies', 3, 1, 1, 'Engineering is the bedrock of progress, pushing the boundaries of what is possible and reshaping the world as we know it. From awe-inspiring skyscrapers to groundbreaking transportation systems, innovative engineering solutions are propelling us into a future that was once deemed unimaginable.

One remarkable engineering marvel that has captivated the world is the field of renewable energy. With the pressing need to transition to sustainable sources, engineers have harnessed the power of wind, solar, and hydroelectric energy to meet our growing demands while minimizing environmental impact. The development of advanced solar panels, wind turbines, and energy storage systems are revolutionizing the way we generate and consume electricity.

Another transformative technology on the horizon is autonomous vehicles. Engineers are redefining transportation by combining artificial intelligence, robotics, and sensor technologies to create self-driving cars, trucks, and drones. These vehicles have the potential to enhance road safety, reduce congestion, and transform the way we travel and transport goods.

In the realm of materials engineering, scientists are pushing the boundaries to develop stronger, lighter, and more sustainable materials. From advanced composites to nanomaterials, these innovations are enabling the construction of durable infrastructure, efficient vehicles, and next-generation electronics.

The convergence of engineering and biotechnology is also shaping the future of healthcare. Engineers are working hand in hand with medical professionals to design cutting-edge medical devices, prosthetics, and bioengineered organs, revolutionizing the field of healthcare and improving the quality of life for millions of people.

As we continue to face complex challenges, engineers are at the forefront, harnessing their creativity, knowledge, and technical expertise to solve the worlds most pressing problems. Through innovation and collaboration, these engineering marvels are paving the way for a more sustainable, connected, and prosperous future.', GETDATE(),'Engineering innovations are shaping our future. Renewable energy, autonomous vehicles, advanced materials, and biotechnology advancements are revolutionizing energy, transportation, infrastructure, and healthcare. These engineering marvels drive progress and promise a remarkable future.', GETDATE()),
	   (15, 'English5.jpg', 5, 'The Beauty and Power of the English Language: A Linguistic Journey', 5, 1, 1, 'With over 1.5 billion speakers worldwide, the English language holds a special place as one of the most influential and widely spoken languages on our planet. Its rich history, vast vocabulary, and nuanced grammar make it a fascinating subject of study and a means of global communication.

English has its roots in Germanic and Romance languages, resulting in a diverse linguistic heritage. From Old English to Middle English and Modern English, the language has evolved over centuries, absorbing vocabulary and grammatical structures from various cultures and civilizations.

One of the unique aspects of English is its extensive vocabulary. With an estimated 170,000 words in current use, English offers a vast array of expressions and nuances. Its flexibility allows for creative wordplay, idioms, and a multitude of synonyms, enriching communication and enabling precise expression of thoughts and emotions.

English literature, from the works of Shakespeare to modern novels, showcases the power and beauty of the language. It has given birth to masterpieces that have shaped literature and influenced cultures worldwide. The intricate combination of words, rhythm, and imagery in poetry and prose has the ability to captivate and transport readers to new worlds.

English has also become the lingua franca of science, technology, and academia. It serves as a bridge between cultures, facilitating global collaboration and exchange of knowledge. From research papers and scientific journals to international conferences, English plays a vital role in the advancement of human understanding.

However, mastering English is not without its challenges. Its irregular spellings, complex verb tenses, and idiomatic expressions can pose difficulties for non-native speakers. Yet, its widespread use and vast resources for learning make it an accessible language for those willing to embark on the journey of proficiency.

As we celebrate the beauty and power of the English language, let us embrace its richness, diversity, and the doors it opens to connect people from all walks of life. It is a language that unites, inspires, and transcends borders, allowing us to share our stories and ideas with the world.', GETDATE(), 'English, a powerful global language, captivates with its rich history, vast vocabulary, and ability to connect people worldwide. Mastering it may pose challenges, but its beauty and accessibility make proficiency achievable. Embrace Englishs unity and expressive power as it transcends borders and fosters global communication.', GETDATE()),
	   (7, 'dongho.jpg', 4, 'Unveiling the Wonders of Mathematics: From Numbers to Infinite Possibilities', 4, 1, 1, 'Mathematics, often referred to as the language of the universe, holds an awe-inspiring allure that has captivated scholars and thinkers for centuries. From the elegance of numbers to the depths of complex equations, mathematics unlocks the secrets of the universe and offers a profound understanding of the world we inhabit.

At its core, mathematics is a tool for quantifying and measuring the world around us. From counting objects to calculating distances, it provides a foundation for logical reasoning and problem-solving. The beauty of mathematics lies not only in its precision and accuracy but also in its ability to reveal patterns and relationships that shape our reality.

One of the fundamental branches of mathematics is arithmetic, which deals with numbers and basic operations. It forms the building blocks for more advanced areas such as algebra, geometry, calculus, and beyond. Through these disciplines, mathematicians have unlocked new vistas of knowledge, enabling groundbreaking advancements in science, engineering, and technology.

Geometry, for instance, explores the properties and relationships of shapes and spatial dimensions. It is the language of architects, allowing them to design magnificent structures with precise measurements and proportions. In physics, calculus becomes indispensable, enabling scientists to describe the motion of celestial bodies, model the behavior of complex systems, and unravel the mysteries of the universe.

Mathematics also delves into the realms of abstract concepts and infinite possibilities. Set theory and logic explore the foundations of mathematical thinking, while number theory and abstract algebra delve into the intricacies of numbers and their properties. These areas not only contribute to the theoretical framework of mathematics but also find practical applications in cryptography, computer science, and data analysis.

While mathematics can be challenging, its rewards are boundless. It nurtures critical thinking skills, enhances problem-solving abilities, and encourages analytical reasoning. It fosters a mindset of inquiry and curiosity, inviting us to explore the limitless potential of the world through the lens of numbers, shapes, and equations.

So let us embark on a journey of discovery, where the wonders of mathematics unfold before our eyes. From the simplicity of arithmetic to the profound elegance of complex mathematical structures, let us embrace the beauty and power of this universal language that unveils the mysteries of our existence.', GETDATE(), 'Mathematics: the universal language unveiling profound insights, limitless possibilities. From arithmetic to advanced fields, it fuels progress, fosters critical thinking, and unravels mysteries. Embrace its beauty and power.', GETDATE()),
	   (10, '1687315937801.jpg', 5, 'The Future of Artificial Intelligence in Healthcare', 3, 1, 1, 'Artificial Intelligence (AI) is transforming the healthcare industry in remarkable ways. From diagnosing diseases to personalized treatment plans, AI is revolutionizing patient care. With the ability to process vast amounts of medical data and identify patterns, AI systems are becoming invaluable tools for healthcare professionals.

One area where AI is making significant strides is medical imaging. Deep learning algorithms can analyze medical images with incredible accuracy, aiding in the detection of diseases such as cancer and providing early intervention. AI-powered chatbots and virtual assistants are also enhancing patient engagement and improving healthcare access.

Another promising application of AI in healthcare is precision medicine. By analyzing an individual genetic data and medical history, AI algorithms can predict disease risks, recommend targeted therapies, and optimize treatment plans. This personalized approach has the potential to improve patient outcomes and reduce healthcare costs.

However, the adoption of AI in healthcare is not without challenges. Ensuring data privacy and security, addressing ethical concerns, and integrating AI systems with existing healthcare infrastructure are among the key hurdles. Collaboration between healthcare providers, researchers, and AI experts is crucial to overcome these obstacles and realize the full potential of AI in healthcare.

As AI continues to advance, the future of healthcare looks promising. With AI-driven innovations, we can expect improved diagnosis accuracy, more efficient healthcare delivery, and better patient outcomes. The synergy between human expertise and AI capabilities holds immense potential for transforming healthcare as we know it.', GETDATE(), 'AI is very good', GETDATE()),
	   (8, 'Chemistry2.jpg', 6, 'The Impact of Blockchain Technology on Supply Chain Management', 4, 1, 1, 'Blockchain technology is revolutionizing supply chain management by providing transparency, traceability, and efficiency. Traditionally, supply chains have been plagued by issues such as counterfeit products, lack of trust among stakeholders, and inefficient processes. Blockchain offers solutions to these challenges.

By leveraging distributed ledger technology, blockchain enables real-time tracking and verification of goods throughout the supply chain. Each transaction is recorded on the blockchain, creating an immutable and transparent audit trail. This enhances trust among participants and helps in the detection and prevention of fraud.

Smart contracts, a key feature of blockchain, automate and enforce contractual agreements between parties. This eliminates the need for intermediaries and streamlines processes, leading to cost savings and faster transactions. Additionally, blockchain can enable secure and decentralized storage of product information, ensuring authenticity and reducing the risk of counterfeiting.

The impact of blockchain on supply chain management extends beyond transparency and efficiency. It also enables new business models and collaborations. For example, blockchain can facilitate peer-to-peer trading platforms, where suppliers and customers can directly interact and transact without intermediaries.

As the adoption of blockchain technology in supply chain management continues to grow, it is essential for businesses to stay informed and embrace the potential benefits. While challenges such as scalability and interoperability remain, the transformative power of blockchain in revolutionizing supply chains cannot be ignored.', GETDATE(), 'Brief info about the blog', GETDATE()),
	   (5, '2086110.jpg', 7, 'The Rise of Augmented Reality in Gaming', 1, 1, 1, 'Augmented Reality (AR) is reshaping the gaming industry and taking gaming experiences to a whole new level. Unlike Virtual Reality (VR), which creates a completely immersive virtual world, AR overlays digital elements onto the real world, enhancing the player perception and interaction with their surroundings.

One of the most popular examples of AR gaming is Pokémon Go, where players use their smartphones to catch virtual Pokémon in real-world locations. AR technology adds a layer of interactivity and immersion, making the gaming experience more engaging and dynamic.

AR also opens up new possibilities for multiplayer gaming. Players can see and interact with virtual characters and objects in real-time, creating a shared gaming experience even in different physical locations. This social aspect of AR gaming brings people together and fosters collaboration and competition.

Beyond entertainment, AR gaming has potential applications in education, training, and simulations. It can be used to create interactive learning experiences, simulate real-life scenarios for training purposes, and enhance virtual tours and museum experiences.

As AR technology continues to advance and become more accessible, we can expect even more innovative and immersive gaming experiences. From gamified exercise routines to location-based adventures, the future of AR gaming is bright and full of possibilities.', GETDATE(), 'Augmented Reality in Gaming', GETDATE()),
	   (10, 'btvv.jpg', 8, 'The Power of Data Analytics in Business Decision-Making', 1, 1, 1, 'Data analytics has become a game-changer in the business world, empowering organizations to make data-driven decisions and gain a competitive edge. With the exponential growth of data and advancements in technology, businesses can harness the power of data analytics to unlock valuable insights and drive strategic decision-making.

By analyzing large volumes of structured and unstructured data, businesses can identify patterns, trends, and correlations. This enables them to understand customer behavior, optimize operations, and identify new business opportunities. Data analytics also helps in predicting future outcomes and mitigating risks.

Business intelligence tools and techniques, such as data visualization, dashboards, and predictive modeling, enable businesses to interpret complex data and communicate insights effectively. This empowers decision-makers at all levels to make informed choices and drive organizational growth.

Data analytics is applicable across various industries and business functions. Marketing teams can leverage customer analytics to personalize campaigns and improve customer engagement. Operations teams can optimize supply chain efficiency and reduce costs. Finance teams can use predictive analytics for financial planning and risk management.

As organizations embark on their data analytics journey, it is crucial to establish a strong data infrastructure, ensure data quality and security, and cultivate a data-driven culture. With the right tools, talent, and strategies, businesses can harness the power of data analytics to thrive in today data-driven world.', GETDATE(), 'Data analytics to thrive in today data-driven world', GETDATE());
GO

INSERT INTO [dimension_type]([name])
VALUES ('Group'),
	   ('Domain');	
	
GO
INSERT INTO [dimension]([name], [type_id], description)
VALUES ('Business', 1, 'The "business" dimension encompasses various aspects of commerce, including strategy, finance, marketing, human resources, operations, and entrepreneurship. It involves managing resources, setting goals, understanding markets, and driving growth for organizations to succeed in a competitive environment.'),
	   ('Process', 1, 'The "process" dimension focuses on optimizing workflows, improving efficiency, and achieving specific outcomes through structured and monitored procedures.'),
	   ('People', 1, 'The "people" dimension focuses on individuals, their interactions, and their impact on organizational success. It involves leadership, communication, collaboration, and talent management.'),
       ('Initiating', 2, 'The "initiating" dimension involves starting and launching new endeavors or projects within an organization.'),
       ('Planning', 2, 'The "planning" dimension focuses on developing strategies and organizing resources to achieve desired goals.'),
	   ('Executing', 2, 'The "executing" dimension involves implementing plans and carrying out tasks to achieve desired outcomes.');
GO
INSERT INTO [subject_category]([name])
VALUES ('Math'),
	   ('Physics'),
	   ('Chemistry'),
	   ('Biology'),
	   ('English');

GO

INSERT INTO [subject]([name], category_id, [status], [description], illustration, featured, author_id, modified)
VALUES ('Math 1', 1, 1, 'Mathematics for beginners', 'Math2.png', 1, 10, '2023-06-01'),
	   ('Math 2', 1, 1, 'Mathematics for intermediates', 'Math3.png',1, 10, '2023-06-02'),
	   ('Math 3', 1, 0, 'Mathematics for semi-pros', 'Math4.png', 1, 10, '2023-06-03'),
	   ('Math 4', 1, 1, 'Mathematics for pros', 'Math5.png', 1, 10, '2023-06-04'),
	   ('Math 5', 1, 1, 'Mathematics for gods', 'Math2.png', 1, 10, '2023-06-09'),
	   ('Physics 3', 2, 1, 'Physics for semi-pros', 'Physic3.jpg', 1, 11, '2023-06-06'),
	   ('Physics 4', 2, 1, 'Physics for pros', 'Physic4.jpg', 1, 11, '2023-06-07'),
	   ('Physics 5', 2, 1, 'Physics for gods', 'Physic5.jpg', 1, 11, '2023-06-08'),
	   ('Chemistry 1', 3, 0, 'Chemistry for benginners', 'Chemistry1.jpg', 1, 12, '2023-06-05'),
	   ('Chemistry 2', 3, 1, 'Chemistry for intermediates', 'Chemistry4.jpg', 1, 12, '2023-06-12'),
	   ('Chemistry 3', 3, 1, 'Chemistry for semi-pros', 'Chemistry5.jpg', 1, 12, '2023-09-01'),
	   ('Chemistry 4', 3, 1, 'Chemistry for pros', 'Chemistry1.jpg', 1, 12, '2022-06-01'),
	   ('Chemistry 5', 3, 1, 'Chemistry for gods', 'Chemistry5.jpg', 1, 12, '2013-06-01'),
	   ('Biology 1', 4, 1, 'Biology for beginners', 'Biology1.png', 1, 10, '2023-06-29'),
	   ('Biology 4', 4, 1, 'Biology for pros', 'Biology4.png', 1, 12, '2023-06-09'),
	   ('Biology 5', 4, 1, 'Biology for gods', 'Biology1.png', 1, 11, '2023-06-03'),
	   ('English 1', 5, 0, 'English for beginners', 'English1.jpg', 1, 10, '2023-12-01'),
	   ('English 2', 5, 1, 'English for intermediates', 'English3.jpg', 1, 11, '2022-06-01'),
	   ('English 3', 5, 1, 'English for semi-pros', 'English5.jpg', 1, 10, '2023-06-03'),
	   ('English 4', 5, 1, 'English for pros', 'English1.jpg', 1, 11, '2023-06-30');
GO

INSERT INTO [subject_dimension](subject_id, dimension_id)
VALUES (1, 1),
	   (2, 1),
	   (3, 1),
	   (4, 1),
	   (5, 1),
	   (6, 2),
	   (7, 2),
	   (8, 2),
	   (9, 3),
	   (10, 3),
	   (11, 3),
	   (12, 3),
	   (13, 3),
	   (14, 4),
	   (15, 4),
	   (16, 4),
	   (17, 5),
	   (18, 5),
	   (19, 5),
	   (20, 5);
		
		INSERT INTO [lesson_type]([name])
VALUES ('Subject Topic'),
	   ('Lesson'),
	   ('Quiz');
GO

INSERT INTO [lesson_topic]([name])
VALUES ('Topic 1'),
	   ('Topic 2'),
	   ('Topic 3'),
	   ('Topic 4'),
	   ('Topic 5');
GO

INSERT INTO lesson (subject_id, name, type_id, topic_id, [order], video_link, [status], exam_id)
VALUES (1, 'Lesson 1', 1, 1, 1, 'https://www.youtube.com/watch?v=mahidr64Jfg', 1, null),
	   (6, 'Lesson 2', 2, 2, 2, 'https://www.youtube.com/watch?v=mahidr64Jfg', 1, null),
	   (11, 'Lesson 3', 3, 3, 3, 'https://www.youtube.com/watch?v=mahidr64Jfg', 1, null),
	   (16, 'Lesson 4', 1, 4, 4, 'https://www.youtube.com/watch?v=mahidr64Jfg', 1, null), 
	   (20, 'Lesson 5', 2, 5, 5, 'https://www.youtube.com/watch?v=mahidr64Jfg', 1, null); 

GO
--insert 10 easy multiple-choice maths questions for subject_id =1
INSERT INTO [question](subject_id, content, option_a, option_b, option_c, option_d, answer, created, answer_explaination, level, status, lesson_id)
VALUES (1, 'What is 1+1?', '1', '2', '3', '4', '2', GETDATE(), 'this is explanation for 1+1', 'easy', 1, 1),
	   (1, 'What is 2+2?', '1', '2', '3', '4', '4', GETDATE(), null, 'easy', 1, 1),
	   (1, 'What is 3+3?', '1', '6', '3', '4', '6', GETDATE(), 'this is explanation for 3+3', 'easy', 1, 1),
	   (1, 'What is 4+4?', '1', '8', '3', '4', '8', GETDATE(), 'this is explanation for 4+4', 'easy', 1, 1),
	   (1, 'What is 5+5?', '1', '10', '3', '4', '10', GETDATE(), null, 'easy', 1, 1),
	   (1, 'What is 6+6?', '1', '12', '3', '4', '12', GETDATE(), 'this is explanation for 6+6', 'easy', 1, 1),
	   (1, 'What is 7+7?', '1', '2', '3', '14', '14', GETDATE(), 'this is explanation for 7+7', 'easy', 1, 1),
	   (1, 'What is 8+8?', '16', '2', '3', '4', '16', GETDATE(), 'this is explanation for 8+8', 'easy', 1, 1),
	   (1, 'What is 9+9?', '18', '2', '3', '4', '18', GETDATE(), null, 'easy', 1, 1),
	   (1, 'What is 10+10?', '1', '20', '3', '4', '20', GETDATE(), 'this is explanation for 10+10', 'easy', 1, 1);
--insert 10 medium multiple-choice PHYSICS questions for subject_id =6
INSERT INTO [question](subject_id, content, option_a, option_b, option_c, option_d, answer, created, answer_explaination, level, status, lesson_id)
VALUES (6, 'What is the unit of force?', 'Newton', 'Kilogram', 'Meter', 'Second', 'Newton', GETDATE(), null, 'medium', 1, 2),
	   (6, 'What is the unit of energy?', 'Newton', 'Kilogram', 'Meter', 'Joule', 'Joule', GETDATE(), 'this is explanation for energy', 'medium', 1, 2),
	   (6, 'What is the unit of power?', 'Newton', 'Kilogram', 'Watt', 'Second', 'Watt', GETDATE(), 'this is explanation for power', 'medium', 1, 2),
	   (6, 'What is the unit of pressure?', 'Newton', 'Pascal', 'Meter', 'Second', 'Pascal', GETDATE(), 'this is explanation for pressure', 'medium', 1, 2),
	   (6, 'What is the unit of frequency?', 'Newton', 'Kilogram', 'Meter', 'Hertz', 'Hertz', GETDATE(), 'this is explanation for frequency', 'medium', 1, 2),
	   (6, 'What is the unit of electric charge?', 'Newton', 'Kilogram', 'Coulomb', 'Second', 'Coulomb', GETDATE(), 'this is explanation for electric charge', 'medium', 1, 2),
	   (6, 'What is the unit of electric current?', 'Newton', 'Kilogram', 'Meter', 'Ampere', 'Ampere', GETDATE(), 'this is explanation for electric current', 'medium', 1, 2),
	   (6, 'What is the unit of voltage?', 'Newton', 'Volt', 'Meter', 'Second', 'Volt', GETDATE(), 'this is explanation for voltage', 'medium', 1, 2),
	   (6, 'What is the unit of resistance?', 'Newton', 'Kilogram', 'Meter', 'Ohm', 'Ohm', GETDATE(), 'this is explanation for resistance', 'medium', 1, 2),
	   (6, 'What is the unit of capacitance?', 'Newton', 'Kilogram', 'Farad', 'Second', 'Farad', GETDATE(), 'this is explanation for capacitance', 'medium', 1, 2);
--insert 10 easy chemistry-related questions for subject_id =11
INSERT INTO [question](subject_id, content, option_a, option_b, option_c, option_d, answer, created, answer_explaination, level, status, lesson_id)
VALUES (11, 'What is the atomic number of Hydrogen?', '1', '2', '3', '4', '1', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 3),
	   (11, 'What is the atomic number of Helium?', '1', '2', '3', '4', '2', GETDATE(), 'this is explanation for Helium', 'easy', 1, 3),
	   (11, 'What is the atomic number of Lithium?', '1', '2', '3', '4', '3', GETDATE(), 'this is explanation for Lithium', 'easy', 1, 3),
	   (11, 'What is the atomic number of Beryllium?', '1', '2', '3', '4', '4', GETDATE(), 'this is explanation for Beryllium', 'easy', 1, 3),
	   (11, 'What is the atomic number of Boron?', '1', '5', '3', '4', '5', GETDATE(), null, 'easy', 1, 3),
	   (11, 'What is the atomic number of Carbon?', '1', '2', '6', '4', '6', GETDATE(), 'this is explanation for Carbon', 'easy', 1, 3),
	   (11, 'What is the atomic number of Nitrogen?', '1', '2', '3', '7', '7', GETDATE(), null, 'easy', 1, 3),
	   (11, 'What is the atomic number of Oxygen?', '1', '2', '3', '8', '8', GETDATE(), 'this is explanation for Oxygen', 'easy', 1, 3),
	   (11, 'What is the atomic number of Fluorine?', '1', '2', '3', '9', '9', GETDATE(), null, 'easy', 1, 3),
	   (11, 'What is the atomic number of Neon?', '1', '2', '3', '10', '10', GETDATE(), 'this is explanation for Neon', 'easy', 1, 3);

--insert 10 easy biology-related questions for subject_id =16
INSERT INTO [question](subject_id, content, option_a, option_b, option_c, option_d, answer, created, answer_explaination, level, status, lesson_id)
VALUES (16, 'What is the largest organ in the human body?', 'Skin', 'Heart', 'Brain', 'Liver', 'Skin', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 4),
	   (16, 'What is the powerhouse of the cell?', 'Skin', 'Heart', 'Brain', 'Mitochondria', 'Mitochondria', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 4),
	   (16, 'What is the largest bone in the human body?', 'Skin', 'Heart', 'Femur', 'Liver', 'Femur', GETDATE(), null, 'easy', 1, 4),
	   (16, 'What is the smallest bone in the human body?', 'Skin', 'Stapes', 'Brain', 'Stapes', 'Stapes', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 4),
	   (16, 'What is the largest muscle in the human body?', 'Skin', 'Heart', 'Gluteus Maximus', 'Liver', 'Gluteus Maximus', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 4),
	   (16, 'What is the smallest muscle in the human body?', 'Stapedius', 'Heart', 'Brain', 'Liver', 'Stapedius', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 4),
	   (16, 'What is the largest artery in the human body?', 'Skin', 'Heart', 'Brain', 'Aorta', 'Aorta', GETDATE(), null, 'easy', 1, 4),
	   (16, 'What is the largest vein in the human body?', 'Skin', 'Vena Cava', 'Brain', 'Liver', 'Vena Cava', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 4),
	   (16, 'What is the largest cell in the human body?', 'Skin', 'Heart', 'Ovum', 'Liver', 'Ovum', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 4),
	   (16, 'What is the smallest cell in the human body?', 'Sperm', 'Heart', 'Brain', 'Liver', 'Sperm', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 4);
--insert 10 easy english-related questions for subject_id =21
INSERT INTO [question](subject_id, content, option_a, option_b, option_c, option_d, answer, created, answer_explaination, level, status, lesson_id)
VALUES (20, 'What is the longest word in the English language?', 'Pneumonoultramicroscopicsilicovolcanoconiosis', 'Heart', 'Brain', 'Liver', 'Pneumonoultramicroscopicsilicovolcanoconiosis', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5),
	   (20, 'What is the shortest word in the English language?', 'I', 'Heart', 'Brain', 'Liver', 'I', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5),
	   (20, 'What is the most commonly used word in the English language?', 'Skin', 'The', 'Brain', 'Liver', 'The', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5),
	   (20, 'What is the most commonly used letter in the English language?', 'Skin', 'Heart', 'E', 'Liver', 'E', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5),
	   (20, 'What is the most commonly used noun in the English language?', 'Skin', 'Time', 'Brain', 'Liver', 'Time', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5),
	   (20, 'What is the most commonly used adjective in the English language?', 'Skin', 'Heart', 'Good', 'Liver', 'Good', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5),
	   (20, 'What is the most commonly used verb in the English language?', 'Be', 'Heart', 'Brain', 'Liver', 'Be', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5),
	   (20, 'What is the most commonly used preposition in the English language?', 'Skin', 'Of', 'Brain', 'Liver', 'Of', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5),
	   (20, 'What is the most commonly used adverb in the English language?', 'Skin', 'Not', 'Brain', 'Liver', 'Not', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5),
	   (20, 'What is the most commonly used pronoun in the English language?', 'It', 'Heart', 'Brain', 'Liver', 'It', GETDATE(), 'this is explanation for Hydro', 'easy', 1, 5);

GO
INSERT INTO price_package (name, description, duration, price, sale, [status])
VALUES ('One-day Access', 'You will have 1 day access to the subject', 1, 100000, 0, 1),
	   ('Seven-day Access', 'You will have 7 days access to the subject', 7, 200000, 0.4, 1),
	   ('One-month Access', 'You will have 1 month access to the subject', 30, 300000, 0, 1),
	   ('Three-month Access', 'You will have 3 months access to the subject', 90, 400000, 0, 1),
	   ('One-year Access', 'You will have 1 year access to the subject', 360, 500000, 0, 1);
GO
INSERT INTO registration (subject_id, price_package_id, user_id, created, subject_name, category_id, [status]) 
VALUES (1, 1, 1, GETDATE(), 'Math 1', 1, 1), 
	   (6, 2, 2, GETDATE(), 'Physics 3', 2, 1), 
	   (11, 3, 3, GETDATE(), 'Chemistry 3', 3, 1), 
	   (16, 4, 1, GETDATE(), 'Biology 5', 4, 1), 
	   (20, 5, 2, GETDATE(), 'English 4', 5, 1);
GO

INsert into [exam] (subject_id, name, level, duration, pass_rate, number_of_question, created, mode, description)
VALUES (1, 'Math 1 Exam', 1, '00:02:00', 0.6, 10, GETDATE(), 1, 'This is an exam for Math 1 subject with 5 questions and 90 minutes duration'),
	   (6, 'Physics 3 Exam ', 2, '00:02:00', 0.3, 10, GETDATE(), 1, 'This is an exam for Physics 3 subject with 5 questions and 30 minutes duration'),
	   (11, 'Chemistry 3 Exam ', 4, '00:02:00', 0.5, 10, GETDATE(), 1, 'This is an exam for Chemistry 3 subject with 5 questions and 90 minutes duration'),
	   (16, 'Biology 5 Exam', 3, '00:02:00', 0.7, 10, GETDATE(), 0, 'This is an exam for Biology 5 subject with 5 questions and 30 minutes duration'),
	   (20, 'English 4 Exam', 5, '00:02:00', 0.8, 10, GETDATE(), 0, 'This is an exam for English 4 subject with 5 questions and 90 minutes duration');

GO
--insert 10 questions for exam_id = 1
INSERT INTO [question_exam] (exam_id, question_id, question_order, marks_allocated)
VALUES (1, 1, 1, 1),
	   (1, 2, 2, 1),
	   (1, 3, 3, 1),
	   (1, 4, 4, 1),
	   (1, 5, 5, 1),
	   (1, 6, 6, 1),
	   (1, 7, 7, 1),
	   (1, 8, 8, 1),
	   (1, 9, 9, 1),
	   (1, 10, 10, 1);
--insert 10 questions for exam_id = 2
INSERT INTO [question_exam] (exam_id, question_id, question_order, marks_allocated)
VALUES (2, 11, 1, 1),
	   (2, 12, 2, 1),
	   (2, 13, 3, 1),
	   (2, 14, 4, 1),
	   (2, 15, 5, 1),
	   (2, 16, 6, 1),
	   (2, 17, 7, 1),
	   (2, 18, 8, 1),
	   (2, 19, 9, 1),
	   (2, 20, 10, 1);
--insert 10 questions for exam_id = 3
INSERT INTO [question_exam] (exam_id, question_id, question_order, marks_allocated)
VALUES (3, 21, 1, 1),
	   (3, 22, 2, 1),
	   (3, 23, 3, 1),
	   (3, 24, 4, 1),
	   (3, 25, 5, 1),
	   (3, 26, 6, 1),
	   (3, 27, 7, 1),
	   (3, 28, 8, 1),
	   (3, 29, 9, 1),
	   (3, 30, 10, 1);
--insert 10 questions for exam_id = 4
INSERT INTO [question_exam] (exam_id, question_id, question_order, marks_allocated)
VALUES (4, 31, 1, 1),
	   (4, 32, 2, 1),
	   (4, 33, 3, 1),
	   (4, 34, 4, 1),
	   (4, 35, 5, 1),
	   (4, 36, 6, 1),
	   (4, 37, 7, 1),
	   (4, 38, 8, 1),
	   (4, 39, 9, 1),
	   (4, 40, 10, 1);
--insert 10 questions for exam_id = 5
INSERT INTO [question_exam] (exam_id, question_id, question_order, marks_allocated)
VALUES (5, 41, 1, 1),
	   (5, 42, 2, 1),
	   (5, 43, 3, 1),
	   (5, 44, 4, 1),
	   (5, 45, 5, 1),
	   (5, 46, 6, 1),
	   (5, 47, 7, 1),
	   (5, 48, 8, 1),
	   (5, 49, 9, 1),
	   (5, 50, 10, 1);

GO

INSERT INTO [exam_user] (exam_id, user_id)
VALUES (1, 1),
	   (2, 2),
	   (3, 3);

INSERT INTO [authorization] (role_id, url)
VALUES (1, 'home'),
	   (1, 'userlist'),
	   (1, 'userinformation'),
	   (1, 'addnewsubject'),
	   (1, 'sliderList'),
	   (1, 'editSlider'),
	   (1, 'pricePackage'),
	   (1, 'addnewpricePackage'),
	   (1, 'lessonDetails'),
	   (1, 'subjectLessons'),
	   (1, 'addNewLessonDetails'),
	   (1, 'changeuserprofileadmin'),
	   (1, 'addnewuser'),
	   (1, 'verifyaddnewuser'),
	   (1, 'sliderDetail'),
	   (1, 'changeBlogDetail'),
	   (1, 'changeSliderDetail'),
	   (1, 'editLessonDetails'),
	   (1, 'questionList'),
	   (1, 'subjectdetailae'),
	   (1, 'subjectlistae'),
	   (1, 'editoverviewsubject'),
	   (1, 'editpricepackage'),
	   (1, 'addnewpricepackagedetail'),
	   (1, 'editstatuspricepackage'),
	   (1, 'editdimension'),
	   (1, 'deletedimension'),
	   (1, 'adddimension'),
	   (1, 'registrationList'),
	   (1, 'questionimport'),


	   (2, 'searchByExamName'),
	   (2, 'searchBySubject'),
	   (2, 'simulationExam'),
	   (2, 'home'),
	   (2, 'practiceList'),
	   (2, 'practiceDetails'),
	   (2, 'userlist'),
	   (2, 'userinformation'),
	   (2, 'addnewsubject'),
	   (2, 'quizhandle'),
	   (2, 'scorequiz'),
	   (2, 'pricePackage'),
	   (2, 'addnewpricePackage'),
	   (2, 'lessonDetails'),
	   (2, 'subjectLessons'),
	   (2, 'addNewLessonDetails'),
	   (2, 'reviewquiz'),
	   (2, 'changeuserprofileadmin'),
	   (2, 'addnewuser'),
	   (2, 'verifyaddnewuser'),
	   (2, 'editLessonDetails'),
	   (2, 'questionList'),
	   (2, 'myRegistration'),
	   (2, 'subjectdetailae'),
	   (2, 'subjectlistae'),
	   (2, 'editoverviewsubject'),
	   (2, 'editpricepackage'),
	   (2, 'addnewpricepackagedetail'),
	   (2, 'editstatuspricepackage'),
	   (2, 'editdimension'),
	   (2, 'deletedimension'),
	   (2, 'adddimension'),	 
	   (2, 'editSubmittedRegistedSubject'),
	   (2, 'registrationList'),
	   (2, 'questionimport'),
	   



	   (3, 'searchByExamName'),
	   (3, 'searchBySubject'),
	   (3, 'simulationExam'),
	   (3, 'home'),
	   (3, 'practiceList'),
	   (3, 'practiceDetails'),
	   (3, 'userlist'),
	   (3, 'userinformation'),
	   (3, 'addnewsubject'),
	   (3, 'quizhandle'),
	   (3, 'scorequiz'),   
	   (3, 'addnewpricePackage'),
	   (3, 'lessonDetails'),
	   (3, 'subjectLessons'),
	   (3, 'addNewLessonDetails'),
	   (3, 'reviewquiz'),
	   (3, 'changeuserprofileadmin'),
	   (3, 'addnewuser'),
	   (3, 'verifyaddnewuser'),
	   (3, 'sliderDetail'),
	   (3, 'changeBlogDetail'),
	   (3, 'changeSliderDetail'),
	   (3, 'editLessonDetails'),
	   (3, 'questionList'),
	   (3, 'myRegistration'),
	   (3, 'subjectdetailae'),
	   (3, 'subjectlistae'),
	   (3, 'editoverviewsubject'),
	   (3, 'editpricepackage'),
	   (3, 'addnewpricepackagedetail'),
	   (3, 'editstatuspricepackage'),
	   (3, 'editdimension'),
	   (3, 'deletedimension'),
	   (3, 'adddimension'),	   
	   (3, 'editSubmittedRegistedSubject'),
	   (3, 'registrationList'),
	   (3, 'questionimport'),

	   (4, 'searchByExamName'),
	   (4, 'searchBySubject'),
	   (4, 'simulationExam'),
	   (4, 'home'),
	   (4, 'practiceList'),
	   (4, 'practiceDetails'),
	   (4, 'userlist'),
	   (4, 'userinformation'),
	   (4, 'addnewsubject'),
	   (4, 'quizhandle'),
	   (4, 'scorequiz'),
	   (4, 'pricePackage'),
	   (4, 'addnewpricePackage'),
	   (4, 'reviewquiz'),
	   (4, 'changeuserprofileadmin'),
	   (4, 'addnewuser'),
	   (4, 'verifyaddnewuser'),
	   (4, 'sliderDetail'),
	   (4, 'changeBlogDetail'),
	   (4, 'changeSliderDetail'),
	   (4, 'editLessonDetails'),	   
	   (4, 'myRegistration'),	
	   (4, 'editoverviewsubject'),
	   (4, 'editpricepackage'),
	   (4, 'addnewpricepackagedetail'),
	   (4, 'editstatuspricepackage'),	   
	   (4, 'editSubmittedRegistedSubject'),
	   (4, 'registrationList'),
	   
	   
	   

	   (5, 'home'),
	   (5, 'practiceList'),
	   (5, 'practiceDetails'),
	   (5, 'quizhandle'),
	   (5, 'scorequiz'), 
	   (5, 'reviewquiz'),
	   (5, 'myRegistration'),
	   (5, 'editSubmittedRegistedSubject'),
	   (5, 'registrationList'),

	   (6, 'searchByExamName'),
	   (6, 'searchBySubject'),
	   (6, 'simulationExam'),
	   (6, 'cusHome'),
	   (6, 'practiceList'),
	   (6, 'practiceDetails'),
	   (6, 'userlist'),
	   (6, 'userinformation'),
	   (6, 'addnewsubject'),
	   (6, 'quizhandle'),
	   (6, 'scorequiz'),
	   (6, 'pricePackage'),
	   (6, 'addnewpricePackage'),
	   (6, 'lessonDetails'),
	   (6, 'subjectLessons'),
	   (6, 'addNewLessonDetails'),
	   (6, 'reviewquiz'),
	   (6, 'changeuserprofileadmin'),
	   (6, 'addnewuser'),
	   (6, 'verifyaddnewuser'),
	   (6, 'sliderDetail'),
	   (6, 'changeBlogDetail'),
	   (6, 'changeSliderDetail'),
	   (6, 'editLessonDetails'),
	   (6, 'questionList'),
	   (6, 'myRegistration'),
	   (6, 'subjectdetailae'),
	   (6, 'subjectlistae'),
	   (6, 'editoverviewsubject'),
	   (6, 'editpricepackage'),
	   (6, 'addnewpricepackagedetail'),
	   (6, 'editstatuspricepackage'),
	   (6, 'editdimension'),
	   (6, 'deletedimension'),
	   (6, 'adddimension'),	  
	   (6, 'editSubmittedRegistedSubject'),
	   (6, 'registrationList'),
	   (6, 'questionimport');
	
	INSERT INTO [question_dimension] (question_id, dimension_id)
VALUES (1, 1),(2, 1),(3, 1),(4, 1),(5, 1),(6, 1),(7, 1),(8, 1),(9, 1),(10, 1),
	   (11, 2),(12, 2),(13, 2),(14, 2),(15, 2),(16, 2),(17, 2),(18, 2),(19, 2),(20, 2),
	   (21, 3),(22, 3),(23, 3),(24, 3),(25, 3),(26, 3),(27, 3),(28, 3),(29, 3),(30, 3),
	   (31, 4),(32, 4),(33, 4),(34, 4),(35, 4),(36, 4),(37, 4),(38, 4),(39, 4),(40, 4),
	   (41, 5),(42, 5),(43, 5),(44, 5),(45, 5),(46, 5),(47, 5),(48, 5),(49, 5),(50, 5);
--drop database Quiz_Practice


